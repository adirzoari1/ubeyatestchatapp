import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux'
import store from './src/store'
import { ChatHome,ChatRoom} from './src/screens'
const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ChatHome">
        <Stack.Screen name="ChatHome" component={ChatHome} 
            options={{ title: 'Chats' }}

        />
        <Stack.Screen name="ChatRoom" component={ChatRoom} />
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
}

export default App;
