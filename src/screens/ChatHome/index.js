import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View, Text, FlatList, Image} from 'react-native';
import {useSelector} from 'react-redux';
import _ from 'lodash';
function ChatHome({navigation}) {
  const chatStore = useSelector((state) => state.chat);
  const userStore = useSelector((state) => state.user);
  const [messagesList, setMessagesList] = useState([]);
  useEffect(() => {
    if (chatStore && userStore) {
      const messagesList = _.values(chatStore.messages).reduce((acc, curr) => {
        const lastMessage = _.last(curr);
    
        const date = new Date(lastMessage.time);
        const friendDetails = _.filter(
          userStore.friends,
          (u) => u.userId === curr[0].userId,
        )[0];
        let data = {
          date,
          lastMessage,
          friendDetails,
        };
        return [...acc, data];
      }, []);
      setMessagesList(messagesList);
    }
    return 
  }, [chatStore]);


  const renderStoryImage = ({item,index}) => (
    <View
      style={{
        marginHorizontal: 15,
        alignItems: 'center',
        justifyContent: 'space-around',
      }}
      key={index}
      >
      <View>
        <Image
          style={{width: 60, height: 60, borderRadius: 30}}
          source={{
            uri: item.imageUrl,
          }}
        />
        {item.isOnline && (
          <View
            style={{
              width: 10,
              height: 10,
              borderRadius: 5,
              backgroundColor: '#28df99',
              position: 'absolute',
              zIndex: 1,
              bottom: 5,
              right: 5,
            }}
          />
        )}
      </View>

      <Text style={{color: 'grey'}}>{item.firstName}</Text>
    </View>
  );

  const renderMessage = ({item,index}) => {
    const {friendDetails, lastMessage, date} = item;
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('ChatRoom', {item});
        }}
        key={index}
        >
        <View style={{flexDirection: 'row', alignItems: 'center', margin: 10}}>
          <View style={{marginRight: 10}}>
            <Image
              style={{width: 60, height: 60, borderRadius: 30}}
              source={{
                uri: friendDetails.imageUrl,
              }}
            />
            {item.isOnline && (
              <View
                style={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  backgroundColor: '#28df99',
                  position: 'absolute',
                  zIndex: 1,
                  bottom: 5,
                  right: 5,
                }}
              />
            )}
          </View>
          <View>
            <Text>
              {friendDetails.firstName} {friendDetails.lastName}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text
                numberOfLines={1}
                style={{fontSize: 13, color: 'grey', width: 250}}>
                {lastMessage.message}
              </Text>
              <Text style={{color: 'grey'}}>
                {' '}
                - {date.toLocaleString('default', {month: 'short'})}{' '}
                {date.getDay()}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={{flex: 1}}>
      <View style={{height: 100}}>
        <FlatList
          data={userStore.friends}
          renderItem={renderStoryImage}
          keyExtractor={(item) => item.userId}
          horizontal={true}
        />
      </View>
      <View>
        <FlatList
          data={messagesList}
          renderItem={renderMessage}
          keyExtractor={(item) => item.messageId}
        />
      </View>
    </View>
  );
}

export default ChatHome;
