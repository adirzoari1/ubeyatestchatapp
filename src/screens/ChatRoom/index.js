import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TextInput,
  Dimensions,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {addMessage, setCurrentTypingUser} from '../../store/chat/chat.actions';
import _ from 'lodash';
import faker from 'faker';
const screen = Dimensions.get('screen');
function ChatRoom({route, navigation}) {
  const {item} = route.params;
  const dispatch = useDispatch();
  const chatStore = useSelector((state) => state.chat);
  const userStore = useSelector((state) => state.user);
  const currentTypingUserId = useSelector((state) => state.chat.currentTypingUserId);

  const [messages, setMessages] = useState([]);
  const [friendDetails, setFriendDetails] = useState({});
  const [roomId, setRoomId] = useState(item.lastMessage.messageId);
  const [textMessage, setTextMessage] = useState('');
  useEffect(() => {
    navigation.setOptions({
      title: `${item.friendDetails.firstName} ${item.friendDetails.firstName}`,
    });
    setFriendDetails(item.friendDetails);
    dispatch(setCurrentTypingUser(userStore.profile.userId))
  }, []);

  useEffect(() => {
    if (chatStore && chatStore.messages) {
      setMessages(chatStore.messages[item.friendDetails.userId]);
    }
  }, [chatStore]);

  function sendMessage() {
    let message = {
      userId: currentTypingUserId,
      message: textMessage,
      time: faker.time.recent(),
      imageUrl: faker.image.avatar(),

      messageId: faker.random.uuid(),
    };

    dispatch(addMessage({message, roomId: item.friendDetails.userId}));
    const currentUserId = currentTypingUserId ===item.friendDetails.userId? userStore.profile.userId:item.friendDetails.userId
    dispatch(setCurrentTypingUser(currentUserId))
    setTextMessage('');

  }

  function renderMessage({item}) {
    const isMyMessage = item.userId === userStore.profile.userId;
   
    const imageUrl = isMyMessage
      ? userStore.profile.imageUrl
      : friendDetails.imageUrl;
    return (
      <View
        style={[Styles.wrapperMessage, isMyMessage && Styles.myWrapperMessage]}>
        <View style={[Styles.message, isMyMessage && Styles.myMessage]}>
          <Text
            style={[Styles.textMessage, isMyMessage && Styles.myTextMessage]}>
            {item.message}
          </Text>
        </View>
        <View style={Styles.wrapperImage}>
          <Image
            style={Styles.image}
            source={{
              uri: imageUrl,
            }}
          />
        </View>
      </View>
    );
  }
  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <FlatList
        data={messages}
        renderItem={renderMessage}
        keyExtractor={(item) => item.id}
      />

      <View style={Styles.wrapperSendMessageText}>
        <TextInput
          style={Styles.sendMessageText}
          onChangeText={(text) => {
            setTextMessage(text);
          }}
          onSubmitEditing={sendMessage}
          value={textMessage}
          placeholder="Enter Text..."

        />
      </View>
    </View>
  );
}

const Styles = StyleSheet.create({
  wrapperMessage: {
    padding: 5,
    flexDirection: 'row',
  },
  myWrapperMessage: {
    flexDirection: 'row-reverse',
  },
  wrapperImage: {
    justifyContent: 'flex-end',
    marginHorizontal: 10,
  },
  message: {
    width: screen.width - 100,
    minHeight: 20,
    backgroundColor: '#0779e4',
    borderRadius: 10,
    alignItems: 'flex-start',
    padding: 10,
  },
  myMessage: {
    backgroundColor: '#e9e9e9',
  },
  friendMessage: {},
  textMessage: {
    color: '#fff',
  },
  myTextMessage: {
    color: '#000',
  },
  image: {
    width: 30,
    height: 30,
    borderRadius: 15,
  },
  wrapperSendMessageText: {
    position: 'absolute',
    bottom: 10,
 
  },
  sendMessageText: {
    height: 50,
    width: screen.width,
    fontSize: 14,
    backgroundColor:'#e7e7e7',
    padding:10
  },
});

export default ChatRoom;
