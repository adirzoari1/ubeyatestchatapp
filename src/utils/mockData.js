import faker from 'faker';
import _ from 'lodash';
const friendsMockData = getFriends();


function getFriends() {
  return _.range(50).map(_=> ({
      imageUrl: faker.image.avatar(),
      userId: faker.random.uuid(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      isOnline: faker.random.boolean(),
    }))
};




function getMockMessages(userId) {
  return _.range(1).map((_) => ({
    message: 'Hello Adir',
    time: faker.time.recent(),
    imageUrl: faker.image.avatar(),
    userId,
    messageId: faker.random.uuid()
  }));
}

function getMessagesFriendsMockData() {
  const messages = {}
  _.forEach(friendsMockData,(val,i)=>{
    messages[val.userId] = getMockMessages(val.userId)
  })

  return messages
 
  
}
export {friendsMockData,getMessagesFriendsMockData};
