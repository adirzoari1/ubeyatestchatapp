import {ADD_MESSAGE,SET_CURRENT_TYPING_USER_TURN} from './chat.types';
import {getMessagesFriendsMockData} from '../../utils/mockData';
const initialState = {
  messages: getMessagesFriendsMockData(),
  currentTypingUserId: '',
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MESSAGE:
       
    const updatedValues = state.messages[action.payload.roomId].concat(action.payload.message)
      return {
        ...state,
        messages: {
          ...state.messages,
          [action.payload.roomId]: [
            ...updatedValues
          ],
        },
      };
  case SET_CURRENT_TYPING_USER_TURN:
  
    return {
      ...state,
      currentTypingUserId:action.payload
    }
  }
  return state;

};

export default chatReducer;
