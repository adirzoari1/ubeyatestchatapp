import {
    ADD_MESSAGE,
    SET_CURRENT_TYPING_USER_TURN
} from './chat.types'

export const addMessage= (item) => ({ type: ADD_MESSAGE, payload: item })
export const setCurrentTypingUser= (data) => ({ type: SET_CURRENT_TYPING_USER_TURN, payload: data })
