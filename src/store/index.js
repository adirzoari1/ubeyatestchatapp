import { createStore,combineReducers } from 'redux'
import chatReducer from './chat/chat.reducer'
import userReducer from './user/user.reducer'

const reducers = combineReducers({
    chat:chatReducer,
    user:userReducer
  
  })
  
const store = createStore(
    reducers
)

export default store