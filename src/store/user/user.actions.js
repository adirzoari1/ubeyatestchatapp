import {
    SET_USER_PROFILE
} from './user.types'

export const setUserProfile= (data) => ({ type: SET_USER_PROFILE, payload: data })
