import { SET_USER_PROFILE } from "./user.types"
import faker from 'faker'
import { friendsMockData } from '../../utils/mockData'
const initialState = {
    profile:{
      userId:'1',
      firstName:'Adir',
      lastName:'Zoari',
      imageUrl:faker.image.avatar()
    },
    friends:friendsMockData
   

}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_PROFILE:{
      return {
        ...state,
        profile:{
          ...action.payload
        }
      }
    }
 
 
  }
  return state
}

export default userReducer